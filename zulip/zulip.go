package zulip

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Zulip struct {
	token string
}

func New(token string) (*Zulip, error) {
	return &Zulip{token: token}, nil
}

func (z *Zulip) ParseAndSendMessage(c *gin.Context) {
	values := c.Request.URL.Query()
	stream := values.Get("stream")
	topic := values.Get("topic")

	var zulipApi string = "https://zulip.letsbit.io/api/v1/external/alertmanager?api_key=" + z.token + "&stream=" + stream + "&topic=" + topic
	response, err := http.Post(
		zulipApi,
		"application/json",
		c.Request.Body,
	)
	if err != nil {
		log.Printf("error when posting to zulip: %s", err.Error())
		return
	}
	defer response.Body.Close()

	var bodyBytes, errRead = ioutil.ReadAll(response.Body)
	if errRead != nil {
		log.Printf("error in parsing zulip answer %s", errRead.Error())
	}
	bodyString := string(bodyBytes)
	log.Printf("Zulip Response: %s", bodyString)
}
