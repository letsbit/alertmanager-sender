package telegram

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"golang.org/x/exp/slices"
)

type Alerts struct {
	Status       string
	Labels       map[string]string
	Annotations  map[string]string
	StartsAt     string
	EndsAt       string
	GeneratorURL string
	Fingerprint  string
}

type Message struct {
	Receiver          string            `json:"receiver"`
	Status            string            `json:"status"`
	Alerts            []Alerts          `json:"alerts"`
	GroupLabels       map[string]string `json:"groupLabels"`
	CommonLabels      map[string]string `json:"commonLabels"`
	CommonAnnotations map[string]string `json:"commonAnnotations"`
	ExternalURL       string            `json:"externalURL"`
	Version           string            `json:"version"`
	GroupKey          string            `json:"groupKey"`
}

type Telegram struct {
	chatId string
	token  string
}

func New(chatId, token string) (*Telegram, error) {
	return &Telegram{chatId: chatId, token: token}, nil
}

func sanitizeString(s string) string {
	s = strings.ReplaceAll(s, "_", "\\_")
	s = strings.ReplaceAll(s, "*", "\\*")
	s = strings.ReplaceAll(s, "[", "\\[")
	s = strings.ReplaceAll(s, "]", "\\]")
	s = strings.ReplaceAll(s, "(", "\\(")
	s = strings.ReplaceAll(s, ")", "\\)")
	s = strings.ReplaceAll(s, "~", "\\~")
	s = strings.ReplaceAll(s, "`", "\\`")
	s = strings.ReplaceAll(s, ">", "\\>")
	s = strings.ReplaceAll(s, "#", "\\#")
	s = strings.ReplaceAll(s, "+", "\\+")
	s = strings.ReplaceAll(s, "-", "\\-")
	s = strings.ReplaceAll(s, "=", "\\=")
	s = strings.ReplaceAll(s, "|", "\\|")
	s = strings.ReplaceAll(s, "{", "\\{")
	s = strings.ReplaceAll(s, "}", "\\}")
	s = strings.ReplaceAll(s, ".", "\\.")
	s = strings.ReplaceAll(s, "!", "\\!")
	return s
}

func selectEmojiByStatus(s string) string {
	var emoji string
	if s == "firing" {
		emoji = "\U0001F6A8"
	} else if s == "resolved" {
		emoji = "\U0001F197"
	} else {
		emoji = ""
	}
	return emoji
}

var (
	excludedLabels      = []string{"endpoint", "instance", "prometheus", "job", "service", "uid"}
	excludedAnnotations = []string{"runbook_url"}
)

func parseMessage(c *gin.Context) ([]string, error) {
	m := Message{}
	if err := c.ShouldBindJSON(&m); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return []string{""}, err
	}

	messages := make([]string, 0)

	for _, a := range m.Alerts {
		message := ""
		emoji := selectEmojiByStatus(a.Status)
		message += "Status: " + emoji + " **" + sanitizeString(a.Status) + "**\nStarted at: " + sanitizeString(a.StartsAt) + "\nEnds at: " + sanitizeString(a.EndsAt) + "\n__Labels__\n"
		for key, value := range a.Labels {
			if slices.Contains(excludedLabels, key) {
				continue
			}

			message += sanitizeString(key) + ": " + sanitizeString(value) + "\n"
		}

		message += "__Annotations__\n"
		for key, value := range a.Annotations {
			if slices.Contains(excludedAnnotations, key) {
				continue
			}

			message += sanitizeString(key) + ": " + sanitizeString(value) + "\n"
		}

		messages = append(messages, message)
	}

	return messages, nil
}

func (t *Telegram) sendMessage(text string) error {
	var telegramApi string = "https://api.telegram.org/bot" + t.token + "/sendMessage"
	response, err := http.PostForm(
		telegramApi,
		url.Values{
			"chat_id":    {t.chatId},
			"text":       {text},
			"parse_mode": {"MarkdownV2"},
		})
	if err != nil {
		log.Printf("error when posting text to the chat: %s", err.Error())
		return err
	}
	defer response.Body.Close()

	bodyBytes, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		log.Printf("error in parsing telegram answer %s", errRead.Error())
		return err
	}
	bodyString := string(bodyBytes)
	log.Printf("Telegram Response: %s", bodyString)

	return nil
}

func (t *Telegram) ParseAndSendMessage(c *gin.Context) {
	messages, err := parseMessage(c)
	if err != nil {
		log.Printf("error when parsing messages for telegram: %s", err.Error())
	}

	for _, message := range messages {
		err = t.sendMessage(message)
		if err != nil {
			log.Printf("error when sending message to telegram: %s", err.Error())
		}
	}
}
