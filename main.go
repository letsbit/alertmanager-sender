package main

import (
	"log"
	"os"

	"example.com/alertmanager-sender/telegram"
	"example.com/alertmanager-sender/zulip"
	"github.com/gin-gonic/gin"
)

func main() {

	t, err := telegram.New(os.Getenv("TELEGRAM_BOT_CHATID"), os.Getenv("TELEGRAM_BOT_TOKEN"))

	if err != nil {
		log.Printf("error when creating telegram bot: %s", err.Error())
	}

	z, err := zulip.New(os.Getenv("ZULIP_BOT_TOKEN"))

	if err != nil {
		log.Printf("error when creating zulip bot: %s", err.Error())
	}

	r := gin.Default()

	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	r.POST("/zulip", z.ParseAndSendMessage)
	r.POST("/telegram", t.ParseAndSendMessage)
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
