APPS     := init registry

all:	$(APPS)

$(APPS):
	go build -o bin/alertmanager-sender main.go

clean:
	rm -rf bin/*

dep: 
	go get -v -d ./...
