ARG VERSION=1.18

FROM golang:${VERSION} AS builder

WORKDIR /build
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
COPY . .

RUN make

# Docker Image

FROM alpine:3.13
WORKDIR alertmanager-sender
# Create a alertmanager-sender user and group first so the IDs get set the same way,
# even as the rest of this may change over time.
RUN addgroup alertmanager-sender && \
  adduser -S -G alertmanager-sender alertmanager-sender

COPY --from=builder /build/bin/alertmanager-sender /bin/alertmanager-sender

# /alertmanager-sender/logs is made available to use as a location to store audit logs, if
# desired; /alertmanager-sender/file is made available to use as a location with the file
# storage backend, if desired; the server will be started with /alertmanager-sender/config as
# the configuration directory so you can add additional config files in that
# location.
RUN mkdir -p /alertmanager-sender/config && chown -R alertmanager-sender:alertmanager-sender /alertmanager-sender

# 8200/tcp is the primary interface that applications use to interact with
# alertmanager-sender.
EXPOSE 8080

CMD ["alertmanager-sender"]
